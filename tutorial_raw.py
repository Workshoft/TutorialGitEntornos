tutorial= """<html>
<h1 text-align=\"center\">TUTORIAL DE GIT</h1>
<h2>Introducción a Git</h2>
<main>

  <p align=\"center\">Git es una herramienta de control de versiones. Los controles de versiones funcionan como una cinta en la que se van grabando los cambios que se hacen a los archivos de un repositorio.</p>

<p align=\"center\">Un repositorio es una carpeta que contiene los archivos que forman un proyecto, que normalmente es un programa pero puede ser un conjunto de archivos de cualquier tipo.
  </p>

  <p align=\"center\">En este tutorial voy a mostrarte como crear un repositorio de Git y sincronizarlo con un repositorio remoto. Además, mostraré los comandos típicos que se usan para desarrollar localmente y para gestionar los conflictos que surgen cuando varios usuarios trabajan en los mismos archivos.</p>

  <p align=\"center\">Después de leerlo serás capaz de gestionar un proyecto por tu cuenta así como trabajar con otros desarrolladores.</p>

  <p align=\"center\">Para empezar sitúate donde quieras crear tu proyecto. En mi caso lo he creado en ~/Documents. Una vez que lo hayas creado sitúate en él.
        <br>
![](imagenes/Selection_002.png)
  </p>

  <p align=\"center\">
    Para empezar a registrar los cambios del proyecto tienes que crear un archivo .git en el que guardarlos:
        <br>
    ![](imagenes/Selection_005.png)
  </p>


  <p align=\"center\">    Crea un archivo HolaMundo.java, en el que podrás empezar a trabajar.
    <br>
        ![](imagenes/Selection_006.png)
  </p>
  <p align=\"center\">    Con un editor de texto escribe el código para imprimir un \"Hola Mundo!\" por pantalla:
      <br>
        ![](imagenes/Selection_008.png)
  </p>

  <p align=\"center\">
    Tener un repositorio en nuestro ordenador está bien, pero lo ideal es tener como referencia un repositorio remoto.
    <br>
    Como vas a trabajar con otro desarrollador en el proyecto, esto es indispensable.
  </p>

  <p align=\"center\">
    Antes de subir tu primer commit tienes que configurar git para subir los commits con tu nombre y cuenta de usuario.
      <br>
      ![](imagenes/Selection_010.png)
      <br>
      Ve a la página inicial de Gitlab y crea un nuevo proyecto:
      ![](imagenes/Selection_036.png)
      <br>
      El nombre del proyecto será \"TutorialGitEntornos\".
      ![](imagenes/Selection_037.png)
      Una vez que has creado el proyecto, dile a tu repositorio local dónde se va a enviar cada vez que lo actualices.
      <br>
      ![](imagenes/Selection_035.png)
  </p>
  <p align=\"center\">
    Ahora, haz saber a git qué archivos quieres añadir al commit, con el comando add. Al ser el primer commit añade todo lo que hay en el repositorio.
    <br>
    Es importante añadir mensajes a los commits para poder revisarlos en el futuro. Añade el mensaje con la opción -m seguida del mensaje entre comillas dobles.
      <br>
        ![](imagenes/Selection_009.png)
  </p>
  <p align=\"center\">
    Escribir nuestra cuenta y contraseña cada vez que empujamos cambios es engorroso e inseguro. Genera una clave ssh que permitirá al repositorio reconocer
    la máquina en la que estás trabajando sin necesidad de teclear tus datos.
    <br>
    Para mayor seguridad usa el comando sudo, que hará que solo un usuario root pueda leer la clave. 
      <br>
        ![](imagenes/Selection_011.png)
  </p>
  <p align=\"center\">
    Usando los privilegios de root, imprime la clave y cópiala.
      <br>
        ![](imagenes/Selection_012.png)
  </p>    
  <p align=\"center\">   
      <br>
        ![](imagenes/Selection_015.png)
  </p>
  <p align=\"center\">
    Entra a SSH Keys en Gitlab, pega la clave que has copiado y dale un nombre reconocible. El nombre identificará a tu ordenador.
      <br>
        ![](imagenes/Selection_014.png)
  </p>
  <p align=\"center\">
    Ahora ya puedes empujar el primer commit al repositorio remoto: 
      <br>
        ![](imagenes/Selection_013.png)
  </p>
  <p align=\"center\">
    Es hora de añadir a tu colega al proyecto. Ve a Settings -> Members e invítale.
      <br>
        ![](imagenes/Selection_016.png)
  </p>
  <p align=\"center\">   
      <br>
        ![](imagenes/Selection_017.png)
  </p>
  <p align=\"center\">
      Sus permisos van a ser de desarrollador:
      <br>
        ![](imagenes/Selection_018.png)
  </p>
  
  <p align=\"center\">   
      <br>
        ![](imagenes/Selection_019.png)
  </p>
  <p align=\"center\">
    Para trabajar en el proyecto desde su ordenador, tu colega usará el comando git clone para descargar una copia.      
    <br>
    ![](imagenes/Selection_038.png)
  </p>
<p>Una vez que lo haya hecho tiene que hacer la configuración de git del mismo modo que tú la has hecho.</p>
  <p align=\"center\">
    Después creará una rama, que es una copia del proyecto en la que se puede trabajar 'yéndose por las ramas'.
    El uso de las ramas está en hacer contribuciones a partes del proyecto sin interferir en su versión principal.
    Piensa en las constantes actualizaciones de una aplicación, como las de un videojuego.
  </p>
  <p>
    El comando checkout -b creará la rama y le situará en ella.
    <br>
    ![](imagenes/Selection_020.png)
    <br>
    Para comprobar que se ha hecho bien se puede usar el comando branch:	
    ![](imagenes/Selection_039.png)
 </p>
  <p align=\"center\">
    Los desarrolladores cometemos fallos constantemente. Tu compañero crea un archivo README.md y se da cuenta de que ya existe README.MD.
      <br>
        ![](imagenes/Selection_021.png)
  </p>
  <p align=\"center\">
    Encima se da cuenta cuando ya ha hecho add y commit. Afortunadamente, git sabe que esto pasa a menudo, por lo que con el comando reset se puede volver hasta el commit que queramos,
    descartando los que hayamos hecho después.
      <br>
        ![](imagenes/Selection_022.png)
  </p>
  <p align=\"center\">
    Usando el comando log, tu compañero comprueba que lo ha solucionado. Sus cambios se han deshecho.
      <br>
        ![](imagenes/Selection_023.png)
  </p>
  <p align=\"center\">
    Mientras tanto, tú has estado preparando un tutorial muy chulo usando el archivo README.MD. Haz un montón de cambios en el archivo, haciendo un commit después de cada cambio.
  </p>
  <p align=\"center\">
    Usa el comando log para ver los cambios. Cómo puedes ver, aparecen un montón de mensajes que has hecho refiriéndote a los cambios de un mismo archivo.
    Esto es un rollo para revisar los cambios, ya que la información que aportan es similar y de poco valor. Imagen por aquí, párrafo por allá, etc.
    <br>
    En casos así deberías juntar todos estos cambios en un solo commit. Usa el comando git rebase -i HEAD~ seguido del número de commits que vas a juntar.
    El último commit recibirá todos los cambios de los demás. Para ello tienes que escribir pick a la izquierda del último y squash a la izquierda de los demás.
    Cuando termines presiona CTRL + X, escribe 'y' y presiona ENTER.
    ![](imagenes/Selection_040.png)
    <br>
    ![](imagenes/Selection_026.png)
  </p>
  <p align=\"center\">
    Como no hemos cambiado el mensaje al juntar los commits, el commit que contiene a todos contiene también todos los mensajes. Los commits deberían ser de una sola línea. 
    <br>
    ![](imagenes/Selection_027.png)
  </p>
  <p align=\"center\">
    Por suerte existe un comando para cambiar el mensaje del último commit. Escribe git --amend y escribe un mensaje seguido de '-SQUASH'.
     <br>
     ![](imagenes/Selection_041.png)
  </p>
  
  <p align=\"center\">   
      <br>
        ![](imagenes/Selection_028.png)
  </p>

  <p align=\"center\">   
      <br>
        ![](imagenes/Selection_030.png)
  </p>
  <h2>Resolución de conflictos</h2>
  <p align=\"center\">
    Al intentar empujar este nuevo commit verás que has creado un conflicto con el repositorio remoto, ya que contiene commits que has machacado en uno solo.
      <br>
        ![](imagenes/Selection_031.png)
  </p>
  <p align=\"center\">
    Por defecto, Gitlab protege la rama master para evitar que los desarrolladores sobreescriban el repositorio con commits que no incluyen los del repositorio.
    Sin embargo, eso es exactamente lo que quieres hacer, ya que vas a sobreescribir un montón de commits con uno que los contiene a todos.
    <br>
    Dirígete a Settings -> Repository y expande 'Protected Branches'. Desprotege la rama master.
    ![](imagenes/Selection_032.png)
  </p>
  <p align=\"center\">
    Ahora ya puedes empujar los cambios mediante la opción --force, que descartará los commits que has machacado en el repositorio remoto.
    <br>
    ![](imagenes/Selection_033.png)
  </p>
  <p align=\"center\">
    Para comparar el repositorio remoto con el local se puede usar el comando git status.
    <br>
    ![](imagenes/Selection_034.png)
  </p>
  <p align=\"center\">
    Uno de los conflictos más típicos surge cuando no se actualiza el repositorio local mediante el remoto antes de hacer un commit propio.
    <br>
    ![](imagenes/Selection_043.png)
  </p>
  <p align=\"center\">
    Al intentar empujar los cambios a la rama verás que git te lo impide, porque tu rama local está atrasada con la remota.
    <br>
    ![](imagenes/Selection_045.png)
  </p>
  <p align=\"center\">
    Si haces un pull de la rama remota (es decir, una actualización), git te sugerirá converger tus cambios con los del repositorio online. Es conveniente añadir un mensaje que explique qué cambios han provocado el conflicto y cómo lo has resuelto.
    <br>
    ![](imagenes/Selection_046.png)
  </p>
  <p align=\"center\">
    Cómo ya no vas a trabajar más con la rama y has integrado los cambios que deseas en la rama principal, borra la rama.
    <br>
    ![](imagenes/Selection_047.png)
    ![](imagenes/Selection_048.png)
  </p>

<h2>Últimos consejos</h2>
  <p>Esto es todo por ahora. Si quieres aprender más sobre Git o necesitas resolver un problema que no aparece en este tutorial, mi recomendación es que utilices Stack Overflow o la excelente documentación que se puede leer desde la terminal.
    <br>
    ![](imagenes/Selection_049.png)
    ![](imagenes/Selection_050.png)
  </p>
</main>

</html>"""

